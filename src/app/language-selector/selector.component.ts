import {Component, Input} from '@angular/core';
import {Language} from '../language.interface';

@Component({
  selector: 'language-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css']
})
export class SelectorComponent {

  @Input() isMobile: boolean;
  @Input() languages: Language[];

  constructor() {}

}


