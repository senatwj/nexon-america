import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

import { CustomSelectorComponent } from './custom.component';

@NgModule({
  declarations: [
    CustomSelectorComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    TranslateModule
  ],
  providers: [HttpClient],
  exports: [CustomSelectorComponent]
})
export class CustomSelectorModule { }
