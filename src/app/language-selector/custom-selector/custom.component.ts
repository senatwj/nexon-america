import {Component, Input} from '@angular/core';
import {Language} from '../../language.interface';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'custom-selector',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.css']
})
export class CustomSelectorComponent {

  private _languages: Language[];
  @Input('languages')
  set languageData(languages: Language[]) {
    if (languages) {
      this._languages = languages;
      this.currentLang = this.matchLanguage(this.translate.currentLang);
    }
  }
  get languages() {
    return this._languages;
  }

  public isOpen: boolean = false;
  public currentLang: Language;

  constructor(public translate: TranslateService) {}

  public setLanguage(language: string) {
    this.translate.use(language);
    this.currentLang = this.matchLanguage(language);
  }

  private matchLanguage = (language: string) => this.languages.find(lang => lang.value === language);

}


