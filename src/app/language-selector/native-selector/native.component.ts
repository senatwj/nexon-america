import {Component, Input} from '@angular/core';
import {Language} from '../../language.interface';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'native-selector',
  templateUrl: './native.component.html',
  styleUrls: ['./native.component.css']
})
export class NativeSelectorComponent {

  @Input() languages: Language[];

  constructor(public translate: TranslateService) {}

  public setLanguage(language: string) {
    this.translate.use(language);
  }

}


