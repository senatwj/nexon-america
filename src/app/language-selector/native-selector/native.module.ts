import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

import { NativeSelectorComponent } from './native.component';

@NgModule({
  declarations: [
    NativeSelectorComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    TranslateModule
  ],
  providers: [HttpClient],
  exports: [NativeSelectorComponent]
})
export class NativeSelectorModule { }
