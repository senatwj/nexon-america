import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

import { SelectorComponent } from './selector.component';

import { CustomSelectorModule } from './custom-selector/custom.module';
import { NativeSelectorModule } from './native-selector/native.module';

@NgModule({
  declarations: [
    SelectorComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    TranslateModule,
    CustomSelectorModule,
    NativeSelectorModule
  ],
  providers: [HttpClient],
  exports: [SelectorComponent]
})
export class SelectorModule { }
