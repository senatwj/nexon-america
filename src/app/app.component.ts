import {Component} from '@angular/core';
import {Language} from './language.interface';
import {TranslateService} from '@ngx-translate/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  public isMobile: boolean;

  public languages: Language[] = [
    {name: 'English', value: 'en'},
    {name: 'French', value: 'fr'},
    {name: 'German', value: 'de'},
    {name: 'Spanish', value: 'es'}
  ];

  private defaultLanguage = 'en';

  constructor(public translate: TranslateService, private device: DeviceDetectorService) {
    this.isMobile = device.isMobile();

    const languages = this.languages.map((language: Language) => language.value);
    translate.addLangs(languages);
    translate.setDefaultLang(this.defaultLanguage);

    const browserLanguage = translate.getBrowserLang();
    translate.use(languages.indexOf(browserLanguage) >= 0 ? browserLanguage : this.defaultLanguage);
  }

}
